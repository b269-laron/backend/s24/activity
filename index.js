//Exponent Operator

const firstNum = 2
const secondNum = 3;

let getCube = `${firstNum**secondNum}`;
console.log(`The Cube of ${firstNum} is ${getCube}`); 

//Template Literals


const address = ["258 Washington Ave NW", " California 90011"];
const [houseNumber, city] = address;
console.log(`I live at ${address}`);



let animal = {
		name: "Lolong",
		type: "Saltwater",
		species: "crocodile",
		weight: "1075 kgs",
		measurement: "20 ft 3 in",
		};

let{name, type, species, weight, measurement} = animal
console.log(`${name} was a ${type} ${species}. He weighed at ${weight} with a measurement of ${measurement}.`); 


// const array = [1, 2, 3, 4, 5];

// const sum = array.reduce((accumulator, currentValue) => {
//   return accumulator + currentValue;
// }, 1);

// console.log(sum); // Output: 15

const array = [1, 2, 3, 4, 5, 15];

const sum = array.reduce((accumulator, currentValue) => {
  accumulator += currentValue;
  console.log(currentValue);

}, 1);



class Dog {
	constructor(Name, age, breed) {
		this.Name = Name;
		this.age = age;
		this.breed = breed;
	}
};

const myPet = new Dog();

myPet.Name = "Frankie",
myPet.age = 5,
myPet.breed = "Miniature Dachschund"
console.log(myPet);

        
